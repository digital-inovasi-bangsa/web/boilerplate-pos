import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.express as px
import dash_bootstrap_components as dbc

# the style arguments for the sidebar. We use position:fixed and a fixed width
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "15rem",
    "padding": "2rem 1rem",
    "background-color": "#222",
}

# the styles for the main content position it to the right of the sidebar and
# add some padding.
CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}

sidebar = html.Div(
    [
        html.H2("Dashboard", className="display-5 text-white"),
        html.Hr(),
        html.P(
            "Visualization of report", className="lead text-white"
        ),
        dbc.Nav(
            [
                dbc.NavLink("Dashboard", href="/dashboard/", active="exact"),
                dbc.NavLink("Back", href="/", active="exact", external_link=True)
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)

content = html.Div([
      html.H3(
        children='Report Data All of Time', className='text-center mb-4'
        ),

    html.Div(children=[
        html.Div(children=[
            dcc.Dropdown(
                id='my-dropdown',
                options=[
                    {'label': 'Filter by Quantity', 'value': 'Qty'},
                    {'label': 'Filter by Income', 'value': 'Income'},
                    {'label': 'Filter by Profit', 'value': 'Profit'},
                    {'label': 'Filter by Transaction', 'value': 'Transaction'}
                ],
                value='Profit'
            ),

            dcc.Graph(
                id='my-graph'
            ),
        ], className='col-8'),
        html.Div(children=[
            dcc.Dropdown(
                id='pie-chart-dropdown',
                options=[
                    {'label': 'Filter by Product Name', 'value': 'name'},
                    {'label': 'Filter by Product Categories', 'value': 'categories'},
                ],
                value='name'
            ),

            dcc.Graph(
                id='pie-chart'
            ),
        ], className='col-4'),
    ], className='row'),
], id="page-content", style=CONTENT_STYLE)

layout = html.Div([dcc.Location(id="url"), sidebar, content])