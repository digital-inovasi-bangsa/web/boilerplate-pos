from flask import render_template, flash, redirect, url_for, request
from flask_security import login_required, roles_accepted
from app.mod_dashboard import bp
from flask_babel import _
from ..decorators import confirmation
from app.mod_pos.models import Sales, Items
from app.mod_user.models import User
from app.mod_product.models import Product
from app.mod_category.models import ProductCategory

@bp.route("/")
@login_required
@confirmation
@roles_accepted("Developer", "Admin", "Cashier")
def index():
    sales = Sales.gets_with_limit(10)
    total_user = User.count()
    total_sales = Sales.count()
    total_product = Product.count()
    total_product_category = ProductCategory.count()
    total_price_all_sales = Sales.sum_of_all_sales()
    total_qty_all_sales = Sales.sum_of_all_qty()
    total_profit_all_sales = Sales.sum_of_all_profit()
    limit_stocks = Product.get_product_with_limit_stock()
    most_buys = Items.most_buy_product_by_name()
    return render_template("main/dashboard.html", title=_("Dashboard"), sales=sales, total_user=total_user, total_sales=total_sales, total_product=total_product, total_product_category=total_product_category, total_price_all_sales=total_price_all_sales, total_qty_all_sales=total_qty_all_sales, total_profit_all_sales=total_profit_all_sales, limit_stocks=limit_stocks, most_buys=most_buys)


@bp.route("/blank")
@login_required
@confirmation
@roles_accepted("Developer")
def blank():
    return render_template("main/blank_template.html", title=_("Blank Template"))


@bp.route("/docs/dashboard")
@login_required
@confirmation
@roles_accepted("Developer")
def docs_dashboard():
    return render_template("main/theme_docs/dashboard.html", title=_("Dashboard Docs"))


@bp.route("/docs/charts")
@login_required
@confirmation
@roles_accepted("Developer")
def docs_charts():
    return render_template("main/theme_docs/charts.html", title=_("Charts Docs"))


@bp.route("/docs/tables")
@login_required
@confirmation
@roles_accepted("Developer")
def docs_tables():
    return render_template("main/theme_docs/tables.html", title=_("Tables Docs"))


@bp.route("/docs/forms")
@login_required
@confirmation
@roles_accepted("Developer")
def docs_forms():
    return render_template("main/theme_docs/forms.html", title=_("Forms Docs"))


@bp.route("/docs/bs-element")
@login_required
@confirmation
@roles_accepted("Developer")
def docs_bs_element():
    return render_template(
        "main/theme_docs/bs_element.html", title=_("Bootstrap Element Docs")
    )


@bp.route("/docs/bs-grid")
@login_required
@confirmation
@roles_accepted("Developer")
def docs_bs_grid():
    return render_template(
        "main/theme_docs/bs_grid.html", title=_("Bootstrap Grid Docs")
    )

