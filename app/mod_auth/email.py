from flask_mail import Message
from app import create_app, mail
from flask import render_template, current_app
from threading import Thread
from flask_babel import _
from app.email import send_email


def send_password_reset_email(user):
    token = user.get_reset_password_token()
    send_email(
        _("Reset Your Password"),
        sender=current_app.config["MAIL_ADMINISTRATOR"],
        recipients=[user.email],
        text_body=render_template(
            "auth_email/reset_password.txt", user=user, token=token
        ),
        html_body=render_template(
            "auth_email/reset_password.html", user=user, token=token
        ),
    )


def send_confirmation_email(user):
    token = user.generate_confirmation_token()
    send_email(
        _("Confirm Your Email"),
        sender=current_app.config["MAIL_ADMINISTRATOR"],
        recipients=[user.email],
        text_body=render_template(
            "auth_email/confirmation.txt", user=user, token=token
        ),
        html_body=render_template(
            "auth_email/confirmation.html", user=user, token=token
        ),
    )
