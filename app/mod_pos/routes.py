from flask import render_template, url_for, session, request, jsonify, make_response
from flask_security import login_required, roles_accepted
from . import bp
from app.mod_bussines_profile.models import BusinessProfile
from app.mod_category.models import ProductCategory
from app.mod_product.models import Product
from app.mod_bussines_profile.models import BusinessProfile
from .models import Items, Sales
from app.helpers import get_db_image
from app.decorators import confirmation
from flask_babel import _

@bp.route("/<string:filename>", methods=["GET"])
@login_required
@confirmation
def display_image_logo(filename):
    """
    function to load image from local path source
    """
    business_profile = BusinessProfile.get_image(filename)
    return get_db_image(business_profile.business_profile_logo, business_profile.business_profile_logo_name)

@bp.route("/")
@login_required
@confirmation
@roles_accepted("Developer", "Admin", "Cashier")
def index():
    bp = BusinessProfile.get()
    return render_template('pos.html', title=_("Point of Sales"), bp=bp)

@bp.route("/transaction")
@login_required
@confirmation
@roles_accepted("Developer", "Admin")
def transaction():
    sales = Sales.gets()
    return render_template("transactions.html", title=_("View Transaction Data"), sales=sales)

@bp.route("/transaction/<int:id>")
@login_required
@confirmation
@roles_accepted("Developer", "Admin")
def items_transaction(id):
    items = Items.get_by_sales(id)
    sales = Sales.get(id)
    return render_template("transactions-items.html", title=_("View Items Transaction Data"), items=items, sales=sales)

@bp.route("/report/profit")
@login_required
@confirmation
@roles_accepted("Developer", "Admin")
def profit_report():
    profit_reports = Sales.sum_of_all_profit_by_year()
    return render_template("report-profit-by-year.html", title=_("View Profit Report"), profit_reports=profit_reports)

@bp.route("/report/profit/<int:param>")
@login_required
@confirmation
@roles_accepted("Developer", "Admin")
def profit_report_by(param):
    profit_report_by_month = Sales.sum_of_all_profit_by_month(param)
    return render_template("report-profit-by-month.html", title=_("View Profit Report"), profit_report_by_month=profit_report_by_month)

@bp.route("/report/profit/<int:year>/<int:month>")
@login_required
@confirmation
@roles_accepted("Developer", "Admin")
def profit_report_by_day(year, month):
    profit_report_by_day = Sales.sum_of_all_profit_by_date(year, month)
    return render_template("report-profit.html", title=_("View Profit Report"), profit_report_by_day=profit_report_by_day)

@bp.route("/report/transaction")
@login_required
@confirmation
@roles_accepted("Developer", "Admin")
def transaction_reports():
    transaction_reports = Sales.transaction_report_by_year()
    return render_template("report-transaction.html", title=_("View Transaction Report"), transaction_reports=transaction_reports)

@bp.route("/report/transaction/<int:year>")
@login_required
@confirmation
@roles_accepted("Developer", "Admin")
def transaction_report_by_month(year):
    transaction_reports = Sales.transaction_report_by_month(year)
    return render_template("report-transaction-by-month.html", title=_("View Transaction Report"), transaction_reports=transaction_reports)

@bp.route("/report/transaction/<int:year>/<int:month>")
@login_required
@confirmation
@roles_accepted("Developer", "Admin")
def transaction_report_by_date(year, month):
    transaction_reports = Sales.transaction_report_by_date(year, month)
    return render_template("report-transaction-by-date.html", title=_("View Transaction Report"), transaction_reports=transaction_reports)

