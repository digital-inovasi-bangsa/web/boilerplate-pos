from flask import Blueprint

bp = Blueprint("mod_pos", __name__, template_folder="pos_templates")

from . import routes
