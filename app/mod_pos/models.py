from app import db
from datetime import datetime
from app.mod_product.models import Product
from app.mod_category.models import ProductCategory

class Items(db.Model):
    __tablename__ = "items"

    items_id = db.Column(db.Integer, primary_key=True)
    qty = db.Column(db.Integer)
    product_purchase_price = db.Column(db.Float)
    product_selling_price = db.Column(db.Float)
    sub_total = db.Column(db.Float)
    sub_profit = db.Column(db.Float) 
    sales_id = db.Column(db.Integer, db.ForeignKey('sales.sales_id'))
    product_id = db.Column(db.Integer, db.ForeignKey('products.product_id'))

    def __repr__(self):
        """
        Representation function
        """
        return "<Sales Item {}>".format(self.items_id)

    @staticmethod
    def get(param):
        """
        Get point of sales by name.
        @param is id of sales 
        """
        return Items.query.filter_by(items_id=param).first()

    @staticmethod
    def get_by_sales(param):
        """
        Get point of sales by name.
        @param is id of sales 
        """
        return Items.query.filter_by(sales_id=param).all()


    @staticmethod
    def gets():
        """
        Get All Category Data.
        """
        return Items.query.all()

    def create(self):
        """
        Create data from self and commit.
        """
        db.session.add(self)
        db.session.commit()

    def update(self):
        """
        Update category.
        """
        db.session.commit()

    @staticmethod
    def delete(param):
        """
        Delete point of sales by id
        @param is id
        """
        data = Items.get(param)
        db.session.delete(data)
        db.session.commit()

    @staticmethod
    def sum_of_sub_total(param):
        """
        Sum of sub_total filter by @param
        @param is saled_id
        """
        query = db.session.query(db.func.sum(Items.sub_total)).group_by(Items.sales_id==param).all()
        return query

    @staticmethod
    def sum_of_qty(param):
        """
        Sum of quantity filter by @param
        @param is saled_id
        """
        query = db.session.query(db.func.sum(Items.qty)).group_by(Items.sales_id==param).all()
        return query

    @staticmethod
    def sum_of_sub_profit(param):
        """
        Sum of sub_total filter by @param
        @param is saled_id
        """
        query = db.session.query(db.func.sum(Items.sub_profit)).group_by(Items.sales_id==param).all()
        return query
    
    @staticmethod
    def most_buy_product_by_name():
        return Items.query.with_entities(Product.product_name.label("name"), db.func.sum(Items.qty).label("qty")).filter(Items.product_id == Product.product_id).group_by(Product.product_id).limit(10).all()

    @staticmethod
    def most_by_product_by_categories():
        return Items.query.with_entities(ProductCategory.name.label("category"), db.func.sum(Items.qty).label("qty")).filter(Items.product_id == Product.product_id, Product.category_id == ProductCategory.id).group_by(ProductCategory.id).all()

class Sales(db.Model):
    __tablename__ = "sales"

    sales_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_at = db.Column(db.DateTime(), default=datetime.now())
    updated_at = db.Column(db.DateTime())
    total_qty = db.Column(db.Float)
    total_price = db.Column(db.Float)
    total_profit = db.Column(db.Float)
    items = db.relationship('Items', backref='items', lazy=True)

    def __repr__(self):
        """
        Representation function
        """
        return "<Sales {}>".format(self.sales_id)

    @staticmethod
    def get(param):
        """
        Get point of sales by name.
        @param is id of sales 
        """
        return Sales.query.filter_by(sales_id=param).first()

    @staticmethod
    def gets():
        """
        Get All Category Data.
        """
        return Sales.query.order_by(Sales.created_at.desc()).all()

    @staticmethod
    def gets_with_limit(limit):
        """
        Get All Sales Data with :limit
        """
        return Sales.query.order_by(Sales.created_at.desc()).limit(limit)

    def create(self):
        """
        Create data from self and commit.
        """
        db.session.add(self)
        db.session.commit()

    def update(self):
        """
        Update category.
        """
        db.session.commit()

    @staticmethod
    def delete(param):
        """
        Delete point of sales by id
        @param is id
        """
        data = Sales.get(param)
        db.session.delete(data)
        db.session.commit()

    @staticmethod
    def count():
        return Sales.query.count()

    @staticmethod
    def sum_of_all_sales():
        query = Sales.query.with_entities(db.func.sum(Sales.total_price).label("total")).first().total
        return query

    @staticmethod
    def sum_of_all_qty():
        query = Sales.query.with_entities(db.func.sum(Sales.total_qty).label("total")).first().total
        return query

    @staticmethod
    def sum_of_all_profit():
        query = Sales.query.with_entities(db.func.sum(Sales.total_profit).label("total")).first().total
        return query

    @staticmethod
    def sum_of_all_profit_by_date(year, month):
        query = Sales.query.with_entities(db.func.sum(Sales.total_profit).label("total"), db.func.date(Sales.created_at).label("date")).group_by(db.func.date(Sales.created_at)).filter(db.func.extract("year", Sales.created_at) == year, db.func.extract("month", Sales.created_at) == month).all()
        return query

    @staticmethod
    def sum_of_all_profit_by_month(param):
        query = Sales.query.with_entities(db.func.sum(Sales.total_profit).label("total"), db.func.extract("year", Sales.created_at).label("year"), db.func.extract("month", Sales.created_at).label("month")).group_by(db.func.extract("month", Sales.created_at)).filter(db.func.extract("year", Sales.created_at) == param).all()
        return query

    @staticmethod
    def sum_of_all_profit_by_year():
        query = Sales.query.with_entities(db.func.sum(Sales.total_profit).label("total"), db.func.extract("year", Sales.created_at).label("year")).group_by(db.func.extract("year", Sales.created_at)).all()
        return query    

    @staticmethod
    def transaction_report_by_date(year, month):
        return Sales.query.with_entities(Sales.sales_id, db.func.date(Sales.created_at).label("date"), db.func.sum(Sales.total_qty).label("qty"), db.func.sum(Sales.total_price).label("income"), db.func.sum(Sales.total_profit).label("profit")).group_by(db.func.date(Sales.created_at)).filter(db.func.extract("year", Sales.created_at) == year ,db.func.extract("month", Sales.created_at) == month).all()

    @staticmethod
    def transaction_report_by_month(year):
        return Sales.query.with_entities(db.func.extract("year", Sales.created_at).label("year"), db.func.extract("month", Sales.created_at).label("month"), db.func.sum(Sales.total_qty).label("qty"), db.func.sum(Sales.total_price).label("income"), db.func.sum(Sales.total_profit).label("profit"), db.func.count(Sales.sales_id).label("transaction")).group_by(db.func.extract("month", Sales.created_at)).filter(db.func.extract("year", Sales.created_at) == year).all()

    @staticmethod
    def transaction_report_by_year():
        query = Sales.query.with_entities(db.func.extract("year", Sales.created_at).label("year"), db.func.sum(Sales.total_qty).label("qty"), db.func.sum(Sales.total_price).label("income"), db.func.sum(Sales.total_profit).label("profit"), db.func.count(Sales.sales_id).label("transaction")).group_by(db.func.extract("year", Sales.created_at)).all()
        return query

    @staticmethod
    def to_dict_transaction_report():
        data = Sales.query.with_entities(db.func.extract("year", Sales.created_at).label("year"), db.func.extract("month", Sales.created_at).label("month"), db.func.sum(Sales.total_qty).label("qty"), db.func.sum(Sales.total_price).label("income"), db.func.sum(Sales.total_profit).label("profit"), db.func.count(Sales.sales_id).label("transaction")).group_by(db.func.extract("month", Sales.created_at)).all()     
        return data

""" p = Product()
i = Items()
i.products.append(p)
db.session.add(i)
db.session.commit() """