from flask import request, abort
from flask_restx import Resource
from flask import current_app

from .service import SalesService
from .dto import SalesDto
from .utils import Sales, Items
from app.utils import validation_error, err_resp, message, internal_err_resp

api = SalesDto.api
data_resp = SalesDto.data_resp

sales_schema = Sales()
items_schema = Items()


@api.route("/")
@api.response(200, "Sales data successfully sent", data_resp)
@api.response(401, "unauthorized")
@api.response(404, "Sales not found")
class SalesList(Resource):
    @api.doc("list_sales")
    def get(self):
        """List all sales"""
        return SalesService.list_sales_data()


@api.route("/<int:id>")
@api.response(200, "Sales data successfully sent", data_resp)
@api.response(401, "unauthorized")
@api.response(404, "Sales not found")
class SalesGet(Resource):
    @api.doc("Get a spesific sales")
    def get(self, id):
        """ Get a spesific sales data by their id """
        return SalesService.get_sales_data(id)


@api.route("/create")
@api.response(201, "Sales data successfully created", data_resp)
@api.response(401, "unauthorized")
@api.response(400, "Malformed data or validations failed.")
class SalesPost(Resource):

    sales = SalesDto.sales

    @api.doc("Post sales data")
    @api.expect(sales, validate=True)
    def post(self):
        """ Create Sales """
        sales_data = request.get_json()

        errors = sales_schema.validate(sales_data)
        for i in sales_data["items"]:
            errors = items_schema.validate(i)

        if errors:
            return validation_error(False, errors), 400

        return SalesService.post_sales(sales_data)
