from flask_restx import Namespace, fields

class CategoryDto:
    api = Namespace('category', description='Category related operational')

    category = api.model('category', {
        'name': fields.String(required=True, description='Name of Categories'),
        'active': fields.Boolean(description='Status available of Category'),
    })

    data_resp = api.model('category data response', {
        'status': fields.Boolean,
        'message': fields.String,
        'category': fields.Nested(category)
    })