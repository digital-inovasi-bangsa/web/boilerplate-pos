from marshmallow import Schema, fields, ValidationError, validates
from marshmallow.validate import Length
from app.mod_category.schema import CategorySchema
from app.mod_category.models import ProductCategory
from flask_babel import lazy_gettext as _l

class Category(CategorySchema):
    name = fields.Str(required=True, validate=[Length(min=3, max=32), ])
    active = fields.Bool(default=True)

    @validates('name')
    def validate_name(self, name):
        category_name = ProductCategory.get_by_name(name)
        if category_name is not None:
            raise ValidationError("Category {} already available. Please use another name".format(name))

def load_data(category_db_obj):
    category_schema = CategorySchema()

    data = category_schema.dump(category_db_obj)

    return data