from flask import request, abort
from flask_restx import Resource

from .service import CategoryService
from .dto import CategoryDto
from .utils import Category
from app.utils import validation_error

from flask_babel import _

api = CategoryDto.api
data_resp = CategoryDto.data_resp

category_schema = Category()

@api.route('/')
@api.response(200, _('Category data sucessfully sent'), data_resp)
@api.response(401, 'unauthorized')
@api.response(404, _('Category not found'))
class Category(Resource):
    @api.doc('list_category')
    def get(self):
        ''' List all category '''
        return CategoryService.list_category_data()

@api.route('/<int:id>')
@api.param('id', 'The category identifier')
@api.response(200, 'Category data sucessfully sent', data_resp)
@api.response(401, 'unauthorized')
@api.response(404, 'Category not found')
class GetCategory(Resource):
    @api.doc('get_category')
    def get(self, id):
        ''' Get category by slug'''
        return CategoryService.get_category_data(id)       

@api.route("/create")
@api.response(201, "Category data successfully created", data_resp)
@api.response(401, 'unauthorized')
@api.response(400, "Malformed data or validation failed")
class CategoryPost(Resource):
    category = CategoryDto.category

    @api.doc("Post category data")

    @api.expect(category, validate=True)
    def post(self):
        """ Create Category """
        category_data = request.get_json()

        errors = category_schema.validate(category_data)

        if errors:
            return validation_error(False, errors), 400

        return CategoryService.post_category(category_data)