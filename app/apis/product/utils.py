from marshmallow import fields, validates, ValidationError
from marshmallow.validate import Length
from app.mod_product.schema import ProductSchema
from app.mod_product.models import Product as ProductModel

class Product(ProductSchema):
    product_name = fields.Str(required=True, validate=[Length(4, 32)])

    @validates('product_name')
    def validate_product_name(self, product_name):
        product_name = ProductModel.get_by_name(product_name)
        if product_name is not None:
            raise ValidationError("Product {} already available. Please use another name".format(product_name))


def load_data(product_db_obj):
    from app.mod_product.schema import ProductSchema

    product_schema = ProductSchema()

    data = product_schema.dump(product_db_obj)

    return data