import os
from app.apis import sales
from flask import Blueprint
from flask_restx import Api
from flask_babel import _

title = os.getenv("APP_NAME") or "Default App"

bp = Blueprint('api', __name__)
authorizations = {"Bearer": {"type": "apiKey", "in": "header", "name": "Authorization"}} 

api = Api(
    bp, 
    title=title,
    version='1.0',
    description=title + ' API Documentation',
    doc='/doc/',
    authorizations=authorizations,
    security='Bearer'
)

from .product.controller import api as ns_product
from .category.controller import api as ns_category
from .sales.controller import api as ns_sales


api.add_namespace(ns_product, path='/product')
api.add_namespace(ns_category, path='/category')
api.add_namespace(ns_sales, path='/sales')
